﻿using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace EmailSend
{
    class SendEmail
    {
        public Account FromAccount { get; set; }
        public Account ToAccount { get; set; }
        public Task SendEmailAsync(Message message)
        {
            SmtpClient smtp = new SmtpClient("smtp.mail.ru", 25);
            smtp.Credentials = new NetworkCredential(FromAccount.EmailAddress, FromAccount.Password);
            smtp.EnableSsl = true;
            smtp.Timeout = 20000;
            MailMessage mailMessage = new MailMessage(FromAccount.EmailAddress, ToAccount.EmailAddress, message.Subject, message.Body);
            smtp.Send(mailMessage);
            return Task.FromResult(0);
        }
    }
}
